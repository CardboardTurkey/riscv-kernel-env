[[_TOC_]]

# Goal

Run a kernel module in riscv qemu using a custom kernel

# Steps

## Prepare the RAMFS

First lets create some directories. Not all of these are needed but why not:

```bash
sudo mkdir --parents ./RAMFS/{dev,etc,lib,lib64,mnt/root,proc,root,sbin,sys}
sudo cp --archive /dev/{null,console,tty} RAMFS/dev
```

Next we want to place a riscv copy of busybox (statically linked) in
`./RAMFS/bin`. You may have to cross-compile this from source or maybe your
package manager can provide you with a copy. In nix we can do the following:

```bash
buildbox_dir=$(nix-build '<nixpkgs>' --arg crossSystem '(import <nixpkgs/lib>).systems.examples.riscv64' -A pkgsStatic.busybox)
sudo cp -r $buildbox_dir/bin RAMFS
```

Finally pop an `init` script into the ramfs:

```bash
sudo cp src/init/sh/init RAMFS/
```

## Build the kernel

Get kernel dependencies and riscv toolchain. Note the former are to be ran on
your build platform (probably x86). On nix you can use the `shell.nix` I built:

```bash
nix-shell shell.nix
```

Otherwise you can just infer it from the shell file.

I've added kernel v5.19.17 as a submodule.

```bash
cd linux-stable
make ARCH=riscv defconfig
make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc)
cd ../
```

## Build the modules

First build and install the modules that come with the kernel:

```bash
cd linux-stable
make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc) modules
sudo make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc) modules_install INSTALL_MOD_PATH=../RAMFS/
```

### Hello world

In `src/modules/helloworld` there are the ingredients for a "hello world"
module. This can be built and installed to the RAMFS as follows:

```bash
sudo make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc) M=../src/modules/helloworld/
sudo make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc) M=../src/modules/helloworld/ modules_install INSTALL_MOD_PATH=../RAMFS/
```

### Crypto

In `src/modules/cryptosha256` there are the ingredients for a module that
utilises the crypto api. I nicked it from here: https://sysprog21.github.io/lkmpg/#crypto.

To build it we'll have to enable `CONFIG_CRYPTO_SHA256` in the kernel. I do
this with:

```
make ARCH=riscv nconfig
```

And then navigate to `Cryptographic API` and enable
`SHA224 and SHA256 digest algorithm`. You can check this successfully modified
the config with:

```
> grep 'CONFIG_CRYPTO_SHA256=' .config
CONFIG_CRYPTO_SHA256=y
```

Rebuild the kernel and then build and install the crypto module:

```bash
sudo make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc)
sudo make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc) M=../src/modules/cryptosha256/
sudo make ARCH=riscv CROSS_COMPILE=riscv64-unknown-linux-gnu- -j $(nproc) M=../src/modules/cryptosha256/ modules_install INSTALL_MOD_PATH=../RAMFS/
cd ../
```

# Actually run it all

First build the initramfs:

```bash
cd RAMFS
sudo find . | cpio -oHnewc | gzip > ../initramfs.gz
cd ../
```

Now we're at last able to run it in qemu:

```bash
qemu-system-riscv64 -nographic -machine virt -kernel linux-stable/arch/riscv/boot/Image -append "root=/dev/ram init=/init" -initrd initramfs.gz
```

Once in the shell we can load and unload the modules with `modprobe`:

```
> modprobe hello
hello: loading out-of-tree module taints kernel.
Hello world.
> modprobe -r hello
Goodbye world.
> modprobe cryptosha256
cryptosha256: loading out-of-tree module taints kernel.
sha256 test for string: "This is a test"
c7be1ed902fb8dd4d48997c6452f5d7e509fbcdbe2808b16bcf4edce4c07d14e
> modprobe -r cryptosha256
```
